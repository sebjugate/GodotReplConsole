# GodotReplConsole

A quick and dirty REPL debug console I made for a game in Godot. Useful for inspecting node and tweaking their properties while the game is running.

Main commands are cd, ls, props to navigate the SceneTree. Get and set properties of the current node by name.

## A silly example

```
	Debug console started.
	/root> ls
	Listing children of /root
		Game
		Network
		Events
		Console
	/root> cd Console
	/root/Console
	/root/Console> ls
	Listing children of /root/Console
		Output
		HBoxContainer
	/root/Console> cd HBoxContainer/Command
	/root/Console/HBoxContainer/Command
	/root/Console/HBoxContainer/Command> "Hello world, my text is " + text
	Hello world, my text is "Hello world, my text is " + text
	/root/Console/HBoxContainer/Command> props
	Listing properties of /root/Console/HBoxContainer/Command
	.
	.
	.
```