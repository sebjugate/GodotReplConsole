extends VBoxContainer

const RED = 'red'
const GREY = 'grey'
const WHITE = 'white'
const BLUE = '#478CBF'

const HELP = """[table=2]
	[cell]cd <node_path>       [/cell][cell] Navigate down the tree to the specified node.[/cell]
	[cell]find <pattern>       [/cell][cell] Navigate to the node matching <pattern> (uses find_node).[/cell]
	[cell]ls                   [/cell][cell] List children of the current node.[/cell]
	[cell]props                [/cell][cell] List the properties of the current node.[/cell]
	[cell]store <name> <value> [/cell][cell] Store a value for later use.[/cell]
	[cell]<property>           [/cell][cell] Output the value of property on the current node or stored value.[/cell]
	[cell]<property> = <value> [/cell][cell] Set <property> on the current node.[/cell]
	[cell]<expression>         [/cell][cell] Evaluate an expression.[/cell]
[/table]"""

# Expose some functionality
class CommandContext:
	var tree : SceneTree
	var pwd : Node = null
	var vars := {}
	
	func _init(scene_tree : SceneTree):
		tree = scene_tree
		pwd = tree.root
		
		
	func get_path():
		return str(pwd.get_path())
		
		
	func id(id : int):
		var n = instance_from_id(id)
		if n:
			pwd = n
			return get_path()
		else:
			return 'Node not found.'
	
	
	func find(pattern : String):
		var n = tree.root.find_node(pattern, true, false)
		if n:
			pwd = n
			return get_path()
		else:
			return 'Node not found.'
	
	
	func cd(node_path : String):
		if pwd.has_node(node_path):
			pwd = pwd.get_node(node_path)
			return get_path()
		else:
			return 'Node not found.'
			
	
	func ls():
		var out = 'Listing children of %s' % get_path()
		for node in pwd.get_children():
			out += "\n\t%s" % node.get_name()
		return out
		
	
	func set(property : String, value):
		if property in pwd:
			pwd[property] = value
			return 'Property set.'
		else:
			return 'Property does not exist.'
		
	
	func props():
		var out = 'Listing properties of %s' % get_path()
		out += '[table=2]'
		for p in pwd.get_property_list():
			out += "[cell]%s[/cell][cell]%s[/cell]" % [p.name, pwd.get(p.name)]
		out += '[/table]'
		return out
			
	
	func store(name : String, value):
		vars[name] = value
		return 'Value stored.'
	
	
	func _get(property : String):
		if property in pwd:
			return pwd[property]
		if vars.has(property):
			return vars[property]


class RegExAlias:
	var _re : RegEx
	var _sub : String
	
	func _init(pattern : String, sub : String):
		_re = RegEx.new()
		_re.compile(pattern)
		_sub = sub

	func matches(subject : String):
		return _re.search(subject)
		
	func sub(subject : String):
		return _re.sub(subject, _sub)
		

var _command_history := []
var _history_idx := 0

var _aliases := [
	RegExAlias.new('^cd (.*)$', 'cd("$1")'),
	RegExAlias.new('^find (.*)$', 'find("$1")'),
	RegExAlias.new('^ls$', 'ls()'),
	RegExAlias.new('^props$', 'props()'),
	RegExAlias.new('^([a-zA-Z_][a-zA-Z_0-9]*)\\s*=\\s*([^=].*)$', 'set("$1", $2)'),
	RegExAlias.new('^store ([a-zA-Z_][a-zA-Z_0-9]*) (.*)$', 'store("$1", $2)')
]

onready var _context = CommandContext.new(get_tree())
onready var _prompt = find_node('Prompt')
onready var _command = find_node('Command')
onready var _output = find_node('Output')

func _ready():
	_out('Debug console started. Use "help" for help.', BLUE)
	_reset_prompt()


func execute(command : String):
	_out("%s> %s" % [_context.get_path(), command], WHITE)
	_command_history.append(command)
	
	if has_method("_cmd_" + command.to_lower()):
		call("_cmd_" + command.to_lower())
		return
	
	for alias in _aliases:
		if alias.matches(command):
			command = alias.sub(command)
			break
	
	var expr = Expression.new()
	if expr.parse(command) != OK:
		_out('Could not parse command: %s' % expr.get_error_text(), RED)
	else:
		_execute(expr)
	
	_reset_prompt()
	
func _reset_prompt():
	_prompt.text = "%s>" % _context.get_path()

func _cmd_help():
	_out(HELP)


func _cmd_clear():
	_output.clear()
	
	
func _out(s : String, color : String = GREY):
	_output.add_text("\n")
	if color:
		s = '[color=%s]%s[/color]' % [color, s]
	_output.append_bbcode(s)


func _execute(expr : Expression):
	var result = str(expr.execute([], _context))
	if expr.has_execute_failed():
		_out('Execute failed: %s' % [expr.get_error_text()], RED)
	else:
		_out(result)
	
	
func _on_Command_gui_input(event):
	if event is InputEventKey and event.pressed:
		match event.scancode:
			KEY_ENTER:
				execute(_command.text.strip_edges())
				_command.clear()
				_history_idx = 0
			
			KEY_UP:
				_command_history_navigate(1)
				
			KEY_DOWN:
				_command_history_navigate(-1)
				

func _command_history_navigate(offset : int):
	_history_idx = clamp(_history_idx + offset, 0, _command_history.size())
	
	if _history_idx > 0 and _command_history.size() > 0:
		_command.text = _command_history[-_history_idx]


func _on_Console_visibility_changed():
	_command.call_deferred('grab_focus')

